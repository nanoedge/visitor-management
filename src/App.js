import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";
import axios from "axios";
import * as faceapi from "face-api.js";
import { Icon, Input, Header, Button, Menu } from "semantic-ui-react";

//App Components

import PersonalData from "./Components/PersonalData";
import PurposeInput from "./Components/PurposeInput";
import VisiteeInput from "./Components/VisiteeInput";
import Landing from "./Components/Landing";
import Agreement from "./Components/Agreement";
import Recognition from "./Components/Recognition";
import StorageQuery from "./Components/StorageQuery";
import Ending from "./Components/Ending";
import AdminPage from "./Components/AdminPage";

//CSS

import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { newFace: false };
    this.handlePersonalDataSubmit = this.handlePersonalDataSubmit.bind(this);
  }
  componentDidMount() {
    this.loadModels();
  }

  loadModels = async () => {
    const MODEL_URL = "/models";
    await faceapi.loadSsdMobilenetv1Model(MODEL_URL);
    await faceapi.loadFaceLandmarkModel(MODEL_URL);
    await faceapi.loadFaceRecognitionModel(MODEL_URL);
  };

  handlePersonalDataSubmit = personalData => {
    let currentComponent = this;
    axios
      .post("/visitors", personalData)
      .then(function(response) {
        console.log(response);
        currentComponent.setState({ personalData: response.data });
        currentComponent.setState({ visitorId: response.data._id });
        if (currentComponent.state.newFace == true) {
          console.log("submitting new face");
          currentComponent.submitNewFace();
        }
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  handlePurposeInputSubmit = purpose => {
    this.setState({ visitPurpose: purpose });
    console.log(purpose);
  };

  handleVisiteeInputSubmit = visitee => {
    this.setState({ visitee: visitee });
    console.log(visitee);
  };

  handleRecognitionSubmit = visitor => {
    this.setState({ visitorId: visitor });
    console.log(visitor);
  };

  submitNewFace = async () => {
    let face = {};
    face.visitorId = this.state.personalData._id;
    console.log("faces:");
    face.descriptors = "[[" + String(this.state.descriptor) + "]]";
    console.log(JSON.stringify(face));
    console.log(face);
    axios
      .post("/faces", face)
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  handleNewFaceSubmit = newFace => {
    this.setState({ newFace: newFace });
    //console.log(newFace);
  };

  handleDescriptorSubmit = descriptor => {
    this.setState({ descriptor: descriptor });
  };

  StorageQuerySubmit = deleteTime => {
    this.setState({ deleteTime: deleteTime });
  };

  handleVisitSubmit = () => {
    var visit = {};
    visit.visitorId = this.state.visitorId;
    visit.time = Date.now();
    visit.visiteeId = this.state.visitee._id;
    visit.purpose = this.state.visitPurpose;
    console.log(visit);
    axios
      .post("/visits", visit)
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  render() {
    return (
      <div className="App-background">
        <Menu inverted secondary size="massive" icon="labeled">
          <Link to="/">
            <Menu.Item name="Exit">
              <Icon name="close" />
              Exit
            </Menu.Item>
          </Link>
          <Menu.Menu position="right">
            <Link to="/AdminPage">
              <Menu.Item name="Admin">
                <Icon name="user" />
                Admin
              </Menu.Item>
            </Link>
          </Menu.Menu>
        </Menu>

        <div>
          <Switch>
            <Route path="/AdminPage" render={() => <AdminPage />} />
            <Route path="/PersonalData" render={() => <PersonalData onPersonalDataSubmit={this.handlePersonalDataSubmit} />} />
            <Route path="/PurposeInput" render={() => <PurposeInput onPurposeInputSubmit={this.handlePurposeInputSubmit} />} />
            <Route path="/VisiteeInput" render={() => <VisiteeInput onVisiteeInputSubmit={this.handleVisiteeInputSubmit} />} />
            <Route
              path="/Recognition"
              render={() => (
                <Recognition
                  onNewFaceSubmit={this.handleNewFaceSubmit}
                  onDescriptorSubmit={this.handleDescriptorSubmit}
                  onRecognitionSubmit={this.handleRecognitionSubmit}
                />
              )}
            />
            <Route path="/StorageQuery" render={() => <StorageQuery onStorageQuerySubmit={this.handleStorageQuerySubmit} />} />
            <Route path="/Ending" render={() => <Ending onVisitSubmit={this.handleVisitSubmit} />} />
            <Route path="/Agreement" component={Agreement} />
            <Route exact path="/" component={Landing} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
