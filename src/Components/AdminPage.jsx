import React, { Component } from "react";
import ReactTable from "react-table";
import "react-table/react-table.css";
import moment from "moment";
import matchSorter from "match-sorter";
import axios from "axios";
import { Icon, Input, Segment, Button, Container, Menu, Modal, Grid } from "semantic-ui-react";
//import editModal from "./Common/editModal";

class AdminPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visitee: { _id: "Loading", name: "Loading", surname: "Loading", department: "Loading", phone: "Loading", company: "Loading" },
      columns: [
        {
          Header: "Name",
          accessor: "name",
          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
          filterAll: true,
          filterable: true
        },
        {
          Header: "Surname",
          accessor: "surname",
          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["surname"] }),
          filterAll: true,
          filterable: true
        },
        {
          Header: "E-mail",
          accessor: "email",
          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["email"] }),
          filterAll: true,
          filterable: true
        },
        {
          Header: "Company",
          accessor: "company",
          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["company"] }),
          filterAll: true,
          filterable: true
        },
        {
          Header: "Phone",
          accessor: "phone",
          filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["phone"] }),
          filterAll: true,
          filterable: true
        },
        {
          accessor: "_id",
          Cell: ({ value }) => (
            <Button
              negative
              compact
              size="mini"
              onClick={() => {
                this.deleteRow({ value });
              }}
            >
              Delete
            </Button>
          )
        }
      ]
    };
  }

  componentDidMount() {
    axios
      .get("/visitors")
      .then(response => {
        this.setState({ data: response.data });
        this.setState({ visitors: response.data });
        //console.log(this.state.columns);
      })
      .catch(error => console.log(error));

    axios
      .get("/faces")
      .then(response => {
        this.setState({ faces: response.data });
        //console.log(response.data);
      })
      .catch(error => console.log(error));

    axios
      .get("/visitees")
      .then(response => {
        this.setState({ visitees: response.data });
        //console.log(this.state.columns);
      })
      .catch(error => console.log(error));
  }

  createVisitorFullName = visitorId => {
    //console.log(visitorId);
    //let visitors = this.state.visitors;
    //console.log(visitors);
    let visitor = this.state.visitors.find(obj => obj._id === visitorId);
    console.log(visitor);
    if (typeof visitor === "undefined") {
      console.log("NO VISITOR FOUND");
      return "Unknown";
    } else {
      return visitor.name + " " + visitor.surname;
    }
  };
  createVisiteeFullName = visiteeId => {
    let visitee = this.state.visitees.find(obj => obj._id === visiteeId);
    //console.log(visitor);
    if (typeof visitee === "undefined") {
      console.log("NO VISITEE FOUND");
      return "Unknown";
    } else {
      return visitee.name + " " + visitee.surname;
    }
  };

  setVisits = () => {
    axios
      .get("/visits")
      .then(response => {
        const edited = response.data.map(el => ({
          ...el,
          timeStamp: moment(Number(el.time)).format("HH:mm"),
          date: moment(Number(el.time)).format("DD-MM-YYYY"),
          visitorFullName: this.createVisitorFullName(el.visitorId),
          visiteeFullName: this.createVisiteeFullName(el.visiteeId)
          //phone: this.state.visitors.find(obj => obj._id == el.visitorId).phone
        }));
        console.log(edited);
        this.setState({ data: edited });
        //console.log(this.state.columns);
      })
      .catch(error => console.log(error));

    let visitsColumns = [
      {
        Header: "Visitor",
        accessor: "visitorFullName",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["visitorFullName"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Visitee",
        accessor: "visiteeFullName",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["visiteeFullName"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Time",
        accessor: "timeStamp",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["timeStamp"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Date",
        accessor: "date",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["date"] }),
        filterAll: true,
        filterable: true
      }
    ];
    //console.log(columns);
    this.setState({ columns: visitsColumns });
  };

  handleOpenOldVisitee = async visiteeId => {
    this.setState({ modalOpen: true });
    let visitees = this.state.visitees;
    let visitee = visitees.find(obj => obj._id === visiteeId);
    //console.log(visitee);
    this.setState({ visitee: visitee });
  };

  handleEditSubmit = () => {
    console.log("handle edit submit");
    let currentComponent = this;
    axios
      .patch("/visitees/" + this.state.visitee._id, this.state.visitee)
      .then(function(response) {
        console.log(response);
        currentComponent.setVisitees();
      })
      .catch(function(error) {
        console.log(error);
      });

    this.handleClose();
  };

  handleNameInput = input => {
    let visitee = this.state.visitee;
    visitee.name = input.target.value;
    this.setState({ visitee: visitee });
    //console.log(this.state);
  };

  handleSurameInput = input => {
    let visitee = this.state.visitee;
    visitee.surname = input.target.value;
    this.setState({ visitee: visitee });

    //this.setState({ surname: input.target.value });
    //console.log(this.state);
  };

  handleEmailInput = input => {
    let visitee = this.state.visitee;
    visitee.email = input.target.value;
    this.setState({ visitee: visitee });

    //this.setState({ email: input.target.value });
    //console.log(this.state);
  };

  handlePhoneInput = input => {
    let visitee = this.state.visitee;
    visitee.phone = input.target.value;
    this.setState({ visitee: visitee });

    //this.setState({ phone: input.target.value });
    //console.log(this.state);
  };

  handleDepartmentInput = input => {
    let visitee = this.state.visitee;
    visitee.department = input.target.value;
    this.setState({ visitee: visitee });

    //this.setState({ department: input.target.value });
    //console.log(this.state);
  };

  handleFloorInput = input => {
    let visitee = this.state.visitee;
    visitee.floor = input.target.value;
    this.setState({ visitee: visitee });

    //this.setState({ floor: input.target.value });
    //console.log(this.state);
  };

  deleteVisiteeRow = id => {
    console.log("deleting row ...");
    let data = this.state.data;
    data = data.filter(function(obj) {
      return obj._id !== id.value;
    });
    //console.log(data);
    this.setState({ data: data });

    //console.log(id.value);
    axios
      .delete("/visitees/" + id.value)
      .then(response => {
        console.log(response.data);
      })
      .catch(error => console.log(error));
  };

  setVisitees = () => {
    //this.handleItemClick();
    axios
      .get("/visitees")
      .then(response => {
        this.setState({ data: response.data });
        this.setState({ visitees: response.data });
        //console.log(this.state.columns);
      })
      .catch(error => console.log(error));

    let visiteeColumns = [
      {
        Header: "Name",
        accessor: "name",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Surname",
        accessor: "surname",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["surname"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "E-mail",
        accessor: "email",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["email"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Floor",
        accessor: "floor",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["floor"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Phone",
        accessor: "phone",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["phone"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Department",
        accessor: "department",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["department"] }),
        filterAll: true,
        filterable: true
      },
      {
        accessor: "_id",
        Cell: ({ value }) => (
          <div>
            <Modal
              basic
              trigger={
                <Button
                  compact
                  primary
                  size="mini"
                  onClick={() => {
                    this.handleOpenOldVisitee(value);
                  }}
                >
                  edit
                </Button>
              }
              open={this.state.modalOpen}
              onClose={this.handleClose}
              closeOnDimmerClick={false}
            >
              <Modal.Content>
                <div className="Modal">
                  <h1 style={{ color: "white", fontSize: "400%" }}>Enter Visitee Info</h1>
                  <br />
                  <br />

                  <Grid>
                    <Grid.Row centered columns={3}>
                      <Grid.Column>
                        <Input
                          defaultValue={this.state.visitee.name}
                          onChange={this.handleNameInput}
                          size="huge"
                          iconPosition="left"
                          placeholder="Name"
                        >
                          <Icon name="address card" />
                          <input />
                        </Input>
                      </Grid.Column>

                      <Grid.Column>
                        <Input
                          defaultValue={this.state.visitee.surname}
                          onChange={this.handleSurameInput}
                          size="huge"
                          iconPosition="left"
                          placeholder="Surname"
                        >
                          <Icon name="address card outline" />
                          <input />
                        </Input>
                      </Grid.Column>

                      <Grid.Column>
                        <Input
                          defaultValue={this.state.visitee.floor}
                          onChange={this.handleFloorInput}
                          size="huge"
                          iconPosition="left"
                          placeholder="Floor"
                        >
                          <Icon name="building outline" />
                          <input />
                        </Input>
                      </Grid.Column>
                    </Grid.Row>
                    <br />
                    <br />
                    <Grid.Row centered columns={3}>
                      <Grid.Column>
                        <Input
                          defaultValue={this.state.visitee.email}
                          onChange={this.handleEmailInput}
                          size="huge"
                          iconPosition="left"
                          placeholder="Email"
                        >
                          <Icon name="at" />
                          <input />
                        </Input>
                      </Grid.Column>

                      <Grid.Column>
                        <Input
                          defaultValue={this.state.visitee.phone}
                          onChange={this.handlePhoneInput}
                          size="huge"
                          iconPosition="left"
                          placeholder="Phone"
                        >
                          <Icon name="phone" />
                          <input />
                        </Input>
                      </Grid.Column>

                      <Grid.Column>
                        <Input
                          defaultValue={this.state.visitee.department}
                          onChange={this.handleDepartmentInput}
                          size="huge"
                          iconPosition="left"
                          placeholder="Department"
                        >
                          <Icon name="building outline" />
                          <input />
                        </Input>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                  <br />
                  <br />
                  <br />
                  <Grid>
                    <Grid.Row centered columns={2}>
                      <Grid.Column textAlign={"center"}>
                        <Button size="massive" onClick={this.handleClose} negative labelPosition="right" icon="close" content="Cancel" />
                      </Grid.Column>
                      <Grid.Column textAlign={"center"}>
                        <Button size="massive" onClick={this.handleEditSubmit} positive labelPosition="right" icon="checkmark" content="Save" />
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </div>
              </Modal.Content>
            </Modal>
            <Button
              negative
              compact
              size="mini"
              onClick={() => {
                this.deleteVisiteeRow({ value });
              }}
            >
              Delete
            </Button>
          </div>
        )
      }
    ];
    //console.log(columns);
    this.setState({ columns: visiteeColumns });
  };

  setVisitors = () => {
    axios
      .get("/visitors")
      .then(response => {
        this.setState({ data: response.data });
        this.setState({ visitors: response.data });
        //console.log(this.state.columns);
      })
      .catch(error => console.log(error));

    axios
      .get("/faces")
      .then(response => {
        this.setState({ faces: response.data });
        //console.log(response.data);
      })
      .catch(error => console.log(error));

    let visitorColumns = [
      {
        Header: "Name",
        accessor: "name",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["name"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Surname",
        accessor: "surname",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["surname"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "E-mail",
        accessor: "email",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["email"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Company",
        accessor: "company",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["company"] }),
        filterAll: true,
        filterable: true
      },
      {
        Header: "Phone",
        accessor: "phone",
        filterMethod: (filter, rows) => matchSorter(rows, filter.value, { keys: ["phone"] }),
        filterAll: true,
        filterable: true
      },
      {
        accessor: "_id",
        Cell: ({ value }) => (
          <Button
            negative
            compact
            size="mini"
            onClick={() => {
              this.deleteRow({ value });
            }}
          >
            Delete
          </Button>
        )
      }
    ];
    //console.log(columns);
    this.setState({ columns: visitorColumns });
  };

  deleteRow = id => {
    console.log("deleting row ...");
    let data = this.state.data;
    data = data.filter(function(obj) {
      return obj._id !== id.value;
    });
    //console.log(data);
    this.setState({ data: data });

    //console.log(id.value);
    axios
      .delete("/visitors/" + id.value)
      .then(response => {
        console.log(response.data);
      })
      .catch(error => console.log(error));

    //console.log(id.value);
    let faces = this.state.faces;
    let face = faces.find(obj => obj.visitorId == id.value);
    console.log(id.value);
    //console.log(faces);
    console.log(face.visitorId);
    axios
      .delete("/faces/" + String(face._id))
      .then(response => {
        console.log(response.data);
      })
      .catch(error => console.log(error));
  };

  handleOpen = () => {
    let visitee = {};
    this.setState({ visitee: visitee });
    this.setState({ modalOpen: true });
  };

  handleNewVisiteeOpen = () => {
    let visitee = {};
    this.setState({ visitee: visitee });
    this.setState({ modalNewOpen: true });
  };

  handleNewClose = () => {
    //console.log(this.state);
    console.log("new close");
    this.setState({ modalNewOpen: false });
  };

  handleClose = () => {
    //console.log(this.state);
    this.setState({ modalOpen: false });
  };

  handleNewVisiteeSubmit = () => {
    console.log("handle new visitee submit");
    this.handleNewClose();
    console.log(this.state.visitee);
    axios
      .post("/visitees", {
        name: this.state.visitee.name,
        surname: this.state.visitee.surname,
        phone: this.state.visitee.phone,
        email: this.state.visitee.email,
        department: this.state.visitee.department,
        floor: this.state.visitee.floor
      })
      .then(function(response) {
        console.log(response);
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  render() {
    let { activeItem } = this.state;

    return (
      <Container>
        <Segment>
          <Modal
            basic
            trigger={
              <Button
                onClick={() => {
                  this.handleNewVisiteeOpen();
                }}
                primary
                fluid
                size="medium"
              >
                New Visitee +
              </Button>
            }
            open={this.state.modalNewOpen}
            onClose={this.handleNewClose}
            closeOnDimmerClick={false}
          >
            <Modal.Content>
              <div className="Modal">
                <h1 style={{ color: "white", fontSize: "400%" }}>Enter Visitee Info</h1>
                <br />
                <br />

                <Grid>
                  <Grid.Row centered columns={3}>
                    <Grid.Column>
                      <Input
                        //defaultValue={this.state.visitee.name}
                        onChange={this.handleNameInput}
                        size="huge"
                        iconPosition="left"
                        placeholder="Name"
                      >
                        <Icon name="address card" />
                        <input />
                      </Input>
                    </Grid.Column>

                    <Grid.Column>
                      <Input
                        // defaultValue={this.state.visitee.surname}
                        onChange={this.handleSurameInput}
                        size="huge"
                        iconPosition="left"
                        placeholder="Surname"
                      >
                        <Icon name="address card outline" />
                        <input />
                      </Input>
                    </Grid.Column>

                    <Grid.Column>
                      <Input
                        //defaultValue={this.state.visitee.floor}
                        onChange={this.handleFloorInput}
                        size="huge"
                        iconPosition="left"
                        placeholder="Floor"
                      >
                        <Icon name="building outline" />
                        <input />
                      </Input>
                    </Grid.Column>
                  </Grid.Row>
                  <br />
                  <br />
                  <Grid.Row centered columns={3}>
                    <Grid.Column>
                      <Input
                        //defaultValue={this.state.visitee.email}
                        onChange={this.handleEmailInput}
                        size="huge"
                        iconPosition="left"
                        placeholder="Email"
                      >
                        <Icon name="at" />
                        <input />
                      </Input>
                    </Grid.Column>

                    <Grid.Column>
                      <Input
                        //defaultValue={this.state.visitee.phone}
                        onChange={this.handlePhoneInput}
                        size="huge"
                        iconPosition="left"
                        placeholder="Phone"
                      >
                        <Icon name="phone" />
                        <input />
                      </Input>
                    </Grid.Column>

                    <Grid.Column>
                      <Input
                        //defaultValue={this.state.visitee.department}
                        onChange={this.handleDepartmentInput}
                        size="huge"
                        iconPosition="left"
                        placeholder="Department"
                      >
                        <Icon name="building outline" />
                        <input />
                      </Input>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <br />
                <br />
                <br />
                <Grid>
                  <Grid.Row centered columns={2}>
                    <Grid.Column textAlign={"center"}>
                      <Button size="massive" onClick={this.handleNewClose} negative labelPosition="right" icon="close" content="Cancel" />
                    </Grid.Column>
                    <Grid.Column textAlign={"center"}>
                      <Button size="massive" onClick={this.handleNewVisiteeSubmit} positive labelPosition="right" icon="checkmark" content="Save" />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
            </Modal.Content>
          </Modal>

          <Menu fluid widths={3}>
            <Menu.Item name="visitors" active={activeItem === "visitors"} onClick={this.setVisitors} />
            <Menu.Item name="visitees" active={activeItem === "visitees"} onClick={this.setVisitees} />
            <Menu.Item name="visits" active={activeItem === "visits"} onClick={this.setVisits} />
          </Menu>
          <ReactTable data={this.state.data} columns={this.state.columns} defaultPageSize={15} />
        </Segment>
      </Container>
    );
  }
}

export default AdminPage;
