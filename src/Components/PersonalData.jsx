import React, { Component } from "react";
import { Icon, Input, Grid, Button, Message } from "semantic-ui-react";
import { withRouter } from "react-router-dom";
import { Formik, Field, ErrorMessage, Form } from "formik";
import * as Yup from "yup";

class PersonalData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  submitVisitor = () => {
    this.props.onPersonalDataSubmit(this.state);
  };

  handleNameInput = input => {
    this.setState({ name: input.target.value });
    //console.log(this.state);
  };

  handleSurameInput = input => {
    this.setState({ surname: input.target.value });
    //console.log(this.state);
  };

  handleEmailInput = input => {
    this.setState({ email: input.target.value });
    //console.log(this.state);
  };

  handlePhoneInput = input => {
    this.setState({ phone: input.target.value });
    //console.log(this.state);
  };

  handleCompanyInput = input => {
    this.setState({ company: input.target.value });
    //console.log(this.state);
  };

  render() {
    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

    return (
      <div className="App">
        <h1 style={{ color: "white", fontSize: "400%" }}>Enter your personal information</h1>
        <br />
        <br />
        <br />
        <Formik
          initialValues={{
            name: "",
            surname: "",
            phone: "",
            company: "",
            email: ""
          }}
          validationSchema={Yup.object({
            name: Yup.string().required(
              <Message basic negative>
                <Message.Header>Enter a value</Message.Header>
              </Message>
            ),
            surname: Yup.string().required(
              <Message negative>
                <Message.Header>Enter a value</Message.Header>
              </Message>
            ),
            email: Yup.string()
              .email(
                <Message negative>
                  <Message.Header>Invalid Email</Message.Header>
                </Message>
              )
              .required(
                <Message negative>
                  <Message.Header>Enter a value</Message.Header>
                </Message>
              ),
            phone: Yup.string()
              .matches(
                phoneRegExp,
                <Message negative>
                  <Message.Header>Invalid Phone Number</Message.Header>
                </Message>
              )
              .required(
                <Message negative>
                  <Message.Header>Enter a value</Message.Header>
                </Message>
              ),
            company: Yup.string().required(
              <Message negative>
                <Message.Header>Enter a value</Message.Header>
              </Message>
            )
          })}
          onSubmit={fields => {
            console.log(fields);
            this.props.onPersonalDataSubmit(fields);
            this.props.history.push("/PurposeInput");
          }}
          render={({ errors, status, touched }) => (
            <Form>
              <Grid>
                <Grid.Row>
                  <Grid.Column>
                    <Field
                      as={Input}
                      name="name"
                      type="text"
                      className={"form-control" + (errors.name && touched.name ? " is-invalid" : "")}
                      placeholder="Name"
                      icon="address card"
                      iconPosition="left"
                      size="huge"
                    />
                    <ErrorMessage name="name" />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Field
                      as={Input}
                      name="surname"
                      type="text"
                      className={"form-control" + (errors.surname && touched.surname ? " is-invalid" : "")}
                      placeholder="Surame"
                      icon="address card outline"
                      iconPosition="left"
                      size="huge"
                    />
                    <ErrorMessage name="surname" />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Field
                      as={Input}
                      name="email"
                      type="text"
                      className={"form-control" + (errors.email && touched.email ? " is-invalid" : "")}
                      placeholder="Email"
                      icon="at"
                      iconPosition="left"
                      size="huge"
                    />
                    <ErrorMessage name="email" />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Field
                      as={Input}
                      name="phone"
                      type="text"
                      className={"form-control" + (errors.phone && touched.phone ? " is-invalid" : "")}
                      placeholder="Phone"
                      icon="phone"
                      iconPosition="left"
                      size="huge"
                    />
                    <ErrorMessage name="phone" />
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column>
                    <Field
                      as={Input}
                      name="company"
                      type="text"
                      className={"form-control" + (errors.company && touched.company ? " is-invalid" : "")}
                      placeholder="company"
                      icon="building outline"
                      iconPosition="left"
                      size="huge"
                    />
                    <ErrorMessage name="company" />
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <br />
              <br />
              <br />
              <Button type="submit" icon labelPosition="right" size="massive">
                Next
                <Icon name="right arrow" />
              </Button>
            </Form>
          )}
        />
      </div>
    );
  }
}

export default withRouter(PersonalData);
