import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Icon, Input, Header, Button } from "semantic-ui-react";

class StorageQuery extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  submitDeleteTime = () => {};

  render() {
    return (
      <div className="App">
        <h1 style={{ color: "white", fontSize: "400%" }}>Do you want us to delete your data after 7 days ?</h1>
        <Link to="/PersonalData">
          <Button size="massive">yes</Button>
        </Link>
        <Link to="/PersonalData">
          <Button size="massive">no</Button>
        </Link>
      </div>
    );
  }
}

export default StorageQuery;
