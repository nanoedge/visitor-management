import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Icon, Input, Header, Button } from "semantic-ui-react";

class Ending extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="App">
        <Link to="/">
          <Button onClick={this.props.onVisitSubmit} size="massive">
            Submit
          </Button>
        </Link>
      </div>
    );
  }
}

export default Ending;
