import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Icon, Input, Header, Button } from "semantic-ui-react";

class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="App">
        <h1 style={{ color: "white", fontSize: "400%" }}>Welcome to [Company X]</h1>
        <br />
        <h1 style={{ color: "white", fontSize: "300%" }}>Press next to begin.</h1>
        <br />
        <br />
        <br />
        <Link to="/Agreement">
          <Button icon labelPosition="right" size="massive">
            Next
            <Icon name="right arrow" />
          </Button>
        </Link>
      </div>
    );
  }
}

export default Landing;
