import React, { Component } from "react";
import { Segment, Dimmer, Loader, Button } from "semantic-ui-react";
import { Link } from "react-router-dom";
import * as faceapi from "face-api.js";
import Camera from "./Common/Camera";
import axios from "axios";

//var name = "";
//let surname = "";

class Recognition extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonView: "Camera",
      visitor: ""
    };
  }
  componentDidMount() {
    this.getFaces();
  }

  lableFaces = async faces => {
    const labeledFaceDescriptors = await Promise.all(
      faces.map(async face => {
        const label = face.visitorId;
        var descriptors = JSON.parse(face.descriptors);
        //console.log(descriptors);
        let array = descriptors[0];
        let faceDescriptors = [new Float32Array(array)];
        return new faceapi.LabeledFaceDescriptors(label, faceDescriptors);
      })
    );

    this.setState({ faces: labeledFaceDescriptors });
  };

  getFaces = () => {
    axios
      .get("/faces")
      .then(response => {
        console.log(response.data);
        this.lableFaces(response.data);
      })
      .catch(error => console.log(error));
  };

  postFace = async (descriptor, visitorId) => {};

  submitDescriptor = descriptor => {
    this.props.onDescriptorSubmit(descriptor);

    console.log(descriptor);
  };

  handleNewFace = () => {
    this.props.onNewFaceSubmit(true);
  };

  handleNoNewFace = async () => {
    this.props.onNewFaceSubmit(false);
  };

  handleFaceCapture = async baseString => {
    this.setState({ buttonView: "Loading" });
    const response = await fetch(baseString);
    const imageBlob = await response.blob();
    const qryimg = await faceapi.bufferToImage(imageBlob);
    console.log(this.state.faces);

    const FaceDescription = await faceapi
      .detectSingleFace(qryimg)
      .withFaceLandmarks()
      .withFaceDescriptor();

    //console.log(String(FaceDescription.descriptor));

    const faceMatcher = new faceapi.FaceMatcher(this.state.faces);

    if (!FaceDescription) {
      this.setState({ buttonView: "noFace" });
    } else {
      const result = faceMatcher.findBestMatch(FaceDescription.descriptor);
      console.log(result);
      //if (result === "123")
      console.log("handleFace Capture");
      console.log(FaceDescription.descriptor);
      this.submitDescriptor(FaceDescription.descriptor);
      //this.setState({ descriptor: FaceDescription.descriptor });
      axios
        .get("/visitors/single/" + result._label)
        .then(response => {
          //console.log(response.data);
          this.props.onRecognitionSubmit(response.data._id);
          this.setState({ visitor: response.data });
          this.setState({ buttonView: "Recognised" });
        })
        .catch(error => {
          console.log(error);
          this.setState({ buttonView: "notRecognised" });
        });
    }
  };

  renderButtons = () => {
    if (this.state.buttonView === "Camera") {
      return (
        <div>
          <h1 style={{ color: "white", fontSize: "400%" }}>Please take a picture of your face.</h1>
          <br />
          <Camera onCapture={this.handleFaceCapture} />
        </div>
      );
    } else if (this.state.buttonView === "Loading") {
      return <Loader active inline="centered" size="large" />;
    } else if (this.state.buttonView === "Recognised") {
      return (
        <div>
          <h1 style={{ color: "white", fontSize: "400%" }}>
            Are you, {this.state.visitor.name} {this.state.visitor.surname} ?
          </h1>
          <Link to="/PurposeInput">
            <Button onClick={this.handleNoNewFace} size="massive">
              Yes
            </Button>
          </Link>
          <Link to="/PersonalData">
            <Button onClick={this.handleNewFace} size="massive">
              No
            </Button>
          </Link>
        </div>
      );
    } else if (this.state.buttonView === "notRecognised") {
      return (
        <div>
          <h1 style={{ color: "white", fontSize: "400%" }}>Would you like us to store your image for easy future access ?</h1>
          <Link to="/PersonalData">
            <Button onClick={this.handleNewFace} size="massive">
              Yes
            </Button>
          </Link>
          <Link to="/PersonalData">
            <Button onClick={this.handleNoNewFace} size="massive">
              No
            </Button>
          </Link>
        </div>
      );
    } else if (this.state.buttonView === "noFace") {
      return <h1 style={{ color: "white", fontSize: "400%" }}>NO FACE DETECTED</h1>;
    }
  };

  render() {
    return <div className="App">{this.renderButtons()}</div>;
  }
}

export default Recognition;
