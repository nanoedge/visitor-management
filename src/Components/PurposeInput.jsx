import React, { Component } from "react";
import { Icon, Input, Button, Label, Message, Popup } from "semantic-ui-react";
import { withRouter } from "react-router-dom";
import { Formik, Field, ErrorMessage, Form } from "formik";
import * as Yup from "yup";

class PurposeInput extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  submitPurpose = () => {
    this.props.history.push("/VisiteeInput");
  };
  handlePurposeInput = input => {
    this.setState({ purpose: input.target.value });
  };

  render() {
    return (
      <div className="App">
        <h1 style={{ color: "white", fontSize: "400%" }}>Why are you visiting ?</h1>
        <br />
        <br />
        <Formik
          initialValues={{
            purpose: ""
          }}
          validationSchema={Yup.object().shape({
            purpose: Yup.string().required(
              <div>
                <br />
                <Message negative>
                  <Message.Header>No selection made !</Message.Header>
                </Message>
              </div>
            )
          })}
          onSubmit={fields => {
            console.log(fields);
            this.submitPurpose();
            this.props.onPurposeInputSubmit(fields.purpose);
          }}
          render={({ errors, status, touched }) => (
            <Form>
              <Field
                as={Input}
                name="purpose"
                type="text"
                className={"form-control" + (errors.purpose && touched.purpose ? " is-invalid" : "")}
                size="massive"
                list="options"
                placeholder="Choose purpose..."
              />
              <datalist id="options">
                <option value="Appointment" />
                <option value="Contractor" />
                <option value="General" />
              </datalist>

              <ErrorMessage name="purpose" />

              <br />
              <br />
              <br />

              <Button type="submit" icon labelPosition="right" size="massive">
                Next
                <Icon name="right arrow" />
              </Button>
            </Form>
          )}
        />
      </div>
    );
  }
}

export default withRouter(PurposeInput);
