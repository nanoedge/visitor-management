import React, { Component } from "react";
import { Icon, Input, Header, Button } from "semantic-ui-react";
import { Link } from "react-router-dom";

class Agreement extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="App">
        <h1 style={{ color: "white", fontSize: "400%" }}>POPI Agreement</h1>
        <br />
        <h1 style={{ color: "white", fontSize: "300%" }}>Content...</h1>
        <br />
        <br />
        <br />
        <Link to="/Recognition">
          <Button icon labelPosition="right" size="massive">
            I have read the notice
            <Icon name="right arrow" />
          </Button>
        </Link>
      </div>
    );
  }
}

export default Agreement;
