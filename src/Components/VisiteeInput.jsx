import React, { Component } from "react";
import { Icon, Grid, Button, Container, Message } from "semantic-ui-react";
import SearchBar from "./Common/SearchBar";
import axios from "axios";
import { withRouter } from "react-router-dom";

class VisiteeInput extends Component {
  constructor(props) {
    super(props);
    this.state = { data: ["nodata"], error: "error", visitee: "novisitee" };
  }
  componentDidMount() {
    axios
      .get("/visitees")
      .then(response => {
        this.setState({ data: response.data });
        //console.log(response.data);
      })
      .catch(error => console.log(error));
  }

  submitVisitee = () => {
    console.log(!this.state.visitee === "novisitee");
    if (!(this.state.visitee === "novisitee")) {
      console.log("ending");
      this.props.onVisiteeInputSubmit(this.state.visitee);
      this.props.history.push("/Ending");
    }
    //this.setState({ error: "error" });
  };

  updateValue = result => {
    this.setState({ visitee: result });
    console.log(result);
  };

  renderError = () => {
    if (this.state.error === "error") {
      return (
        <div>
          <br />
          <Message negative>
            <Message.Header>No selection made !</Message.Header>
          </Message>
        </div>
      );
    } else if (this.state.error === "noError") {
      return <div />;
    }
  };

  render() {
    return (
      <div className="App">
        <h1 style={{ color: "white", fontSize: "400%" }}>Who are you visiting ?</h1>
        <br />
        <br />
        <br />
        <br />
        <Grid columns={3}>
          <Grid.Column>
            <SearchBar onUpdateValue={this.updateValue} data={this.state.data} />
          </Grid.Column>
          <Grid.Column></Grid.Column>
          <Grid.Column>
            <Button icon onClick={this.submitVisitee} labelPosition="right" size="massive">
              Next
              <Icon name="right arrow" />
            </Button>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default withRouter(VisiteeInput);
