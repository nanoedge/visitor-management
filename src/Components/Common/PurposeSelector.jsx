import React, { Component } from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import { Icon, Input, Button } from "semantic-ui-react";

class PurposeSelector extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Formik
        initialValues={{ purpose: "" }}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
          }, 500);
        }}
        validationSchema={Yup.object().shape({
          purpose: Yup.string().required("Required")
        })}
      >
        {props => {
          const { values, touched, errors, dirty, isSubmitting, handleSubmit, handleBlur, handleReset, handleChange } = props;
          return (
            <form onSubmit={handleSubmit}>
              {errors.purpose && touched.purpose && <div className="input-feedback">{errors.purpose}</div>}

              <Input
                id="purpose"
                onChange={handleChange}
                size="massive"
                list="options"
                placeholder="Choose purpose..."
                value={values.purpose}
                className={errors.purpose && touched.purpose ? "text-input error" : "text-input"}
                type="text"
                onBlur={handleBlur}
              />
              <datalist id="options">
                <option value="Appointment" />
                <option value="Contractor" />
                <option value="General" />
              </datalist>
            </form>
          );
        }}
      </Formik>
    );
  }
}

export default PurposeSelector;
