import _ from "lodash";
import React, { Component } from "react";
import { Search, Button, Icon } from "semantic-ui-react";

const initialState = { isLoading: false, results: [], value: "" };

export default class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    setTimeout(this.handleDataChange, 1000);
  }

  handleDataChange = () => {
    this.setState({ data: this.props.data });
  };

  handleResultSelect = (e, { result }) => {
    this.setState({
      value: result.name + " " + result.surname
    });
    this.props.onUpdateValue(result);
  };

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value });

    setTimeout(() => {
      if (this.state.value.length < 1) return this.setState(initialState);

      const re = new RegExp(_.escapeRegExp(this.state.value), "i");
      const isMatch = result => re.test(result.name);

      const results = this.state.data.filter(isMatch).map(result => ({ ...result, key: result._id, title: result.name }));

      this.setState({
        isLoading: false,
        results: results
      });
    }, 300);
  };

  resultRenderer({ name, surname, department, _id }) {
    return (
      <div id={_id} key={_id}>
        {name + " " + surname + " - " + department}
      </div>
    );
  }

  render() {
    const { isLoading, value, results } = this.state;

    return (
      <Search
        size="massive"
        loading={isLoading}
        onResultSelect={this.handleResultSelect}
        onSearchChange={_.debounce(this.handleSearchChange, 500, {
          leading: true
        })}
        results={results}
        value={value}
        resultRenderer={this.resultRenderer}
        placeholder="Choose visitee..."
      />
    );
  }
}
