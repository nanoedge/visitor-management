import React from "react";
import Webcam from "react-webcam";
import { Button, Grid, Modal } from "semantic-ui-react";

class Camera extends React.Component {
  state = {
    modalOpen: false
  };

  setRef = webcam => {
    this.webcam = webcam;
  };

  capture = () => {
    const baseString = this.webcam.getScreenshot();
    this.handleClose();
    this.props.onCapture(baseString);
  };

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({ modalOpen: false });

  render() {
    const videoConstraints = {
      width: 720,
      height: 720,
      facingMode: "user"
    };

    return (
      <div>
        <Modal
          basic
          trigger={
            <Button size="massive" onClick={this.handleOpen}>
              Take Picture
            </Button>
          }
          open={this.state.modalOpen}
          onClose={this.handleClose}
          closeOnDimmerClick={false}
        >
          <Modal.Content>
            <Grid>
              <Grid.Row centered columns={1}>
                <Grid.Column textAlign={"center"}>
                  <Webcam
                    audio={false}
                    height={600}
                    ref={this.setRef}
                    screenshotFormat="image/jpeg"
                    width={600}
                    videoConstraints={videoConstraints}
                  />
                </Grid.Column>
              </Grid.Row>
              <Grid.Row centered columns={2}>
                <Grid.Column textAlign={"center"}>
                  <Button size="massive" onClick={this.capture} positive labelPosition="right" icon="checkmark" content="Take Picture" />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}

export default Camera;
